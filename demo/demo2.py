from Frenchlib import TextTranslation
app_id = "APPID"
app_key = "APPKEY"
test = TextTranslation.TextTranslation(app_id, app_key)
# 这个是文本翻译的主方法和其他类不一样的是这个需要三个参数(源语言,目标语言,需要翻译的文本），这个顺序不能乱
yuan = "zh"
mubiao = "en"
text = "我是一个好人!"
js = test.start(yuan, mubiao, text)
print(js)